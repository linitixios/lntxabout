# LNTXAbout CHANGELOG

## 0.3.1

- **Fixed** Typo in localization file.

## 0.3.0

- **Added** French localization.

## 0.2.3

- **Fixed** Feedback titles in "confused" and "unhappy" views.

## 0.2.2

- **Fixed** URL schemes for Twitter and Facebook.

## 0.2.1

- **Fixed** Default feedback view controller content.

## 0.2.0

- **Added** `LNTXFeedbackTableViewController`: Feedback view controller.

## 0.1.0

Initial release.

- **Added** `LNTXAbout`: About view controller.