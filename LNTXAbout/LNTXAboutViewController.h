//
//  LNTXAboutViewController.h
//  LNTXAbout
//
//  Created by Damien Rambout on 24/03/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LNTXAboutViewController : UITableViewController

+ (instancetype)instantiate;

@end
