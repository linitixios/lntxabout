//
//  MVSTFeedbackTableViewController.h
//  Movist
//
//  Created by Cador Kevin on 11/03/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, LNTXFeedbackType) {
    LNTXFeedbackTypeHappy,
    LNTXFeedbackTypeConfused,
    LNTXFeedbackTypeUnhappy
};

@class LNTXFeedbackTableViewController;

typedef void(^LNTXFeedbackActionSheetCompletion)(LNTXFeedbackTableViewController *viewController);

@interface LNTXFeedbackTableViewController : UITableViewController

+ (instancetype)instantiateWithType:(LNTXFeedbackType)feedbackType;
+ (void)showFeedbackActionSheetInView:(UIView *)view completion:(LNTXFeedbackActionSheetCompletion)completion;

+ (void)setSupportEmailSubject:(NSString *)subject forFeedbackType:(LNTXFeedbackType)feedbackType;
+ (NSString *)supportEmailSubjectForFeedbackType:(LNTXFeedbackType)feedbackType;
+ (void)setSupportEmailMessage:(NSString *)subject forFeedbackType:(LNTXFeedbackType)feedbackType;
+ (NSString *)supportEmailMessageForFeedbackType:(LNTXFeedbackType)feedbackType;
+ (void)setSupportEmailRecipients:(NSArray *)recipients forFeedbackType:(LNTXFeedbackType)feedbackType;
+ (NSArray *)supportEmailRecipientsForFeedbackType:(LNTXFeedbackType)feedbackType;

@property (nonatomic, readonly) LNTXFeedbackType feedbackType;
@property (nonatomic, copy) NSURL *appStoreURL;
@property (nonatomic, copy) NSString *supportEmailSubject;
@property (nonatomic, copy) NSString *supportEmailMessage;
@property (nonatomic, copy) NSArray *supportEmailRecipients;
@property (nonatomic, copy) NSString *friendEmailSubject;
@property (nonatomic, copy) NSString *friendEmailMessage;
@property (nonatomic, copy) NSString *twitterMessage;
@property (nonatomic, copy) NSString *facebookMessage;


@end
