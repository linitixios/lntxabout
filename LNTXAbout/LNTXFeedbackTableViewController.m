//
//  MVSTFeedbackTableViewController.m
//  Movist
//
//  Created by Cador Kevin on 11/03/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXFeedbackTableViewController.h"

#import <Social/Social.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <LNTXLocalizationKit/LNTXLocalizationKit.h>

static NSMutableDictionary *sSupportEmailSubjects;
static NSMutableDictionary *sSupportEmailMessages;
static NSMutableDictionary *sSupportEmailRecipients;

static NSString * const kLocalizationTable = @"LNTXAbout.local";

@interface LNTXFeedbackActionSheetDelegate : NSObject
<UIActionSheetDelegate>

@property (nonatomic, copy) LNTXFeedbackActionSheetCompletion completion;

@end

static LNTXFeedbackActionSheetDelegate *sActionSheetDelegate;

@interface LNTXFeedbackTableViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation LNTXFeedbackTableViewController

+ (void)initialize {
    
    sSupportEmailSubjects = [NSMutableDictionary dictionaryWithCapacity:3];
    [self setSupportEmailSubject:[NSString stringWithFormat:LNTXLocalizedStringFromTable(@"feedback_contact_default_happy_email_subject",
                                                                                         kLocalizationTable), [self appName]] forFeedbackType:LNTXFeedbackTypeHappy];
    [self setSupportEmailSubject:[NSString stringWithFormat:LNTXLocalizedStringFromTable(@"feedback_contact_default_confused_email_subject",
                                                                                         kLocalizationTable), [self appName]] forFeedbackType:LNTXFeedbackTypeConfused];
    [self setSupportEmailSubject:[NSString stringWithFormat:LNTXLocalizedStringFromTable(@"feedback_contact_default_unhappy_email_subject",
                                                                                         kLocalizationTable), [self appName]] forFeedbackType:LNTXFeedbackTypeUnhappy];
    
    sSupportEmailMessages = [NSMutableDictionary dictionaryWithCapacity:3];
    NSString *appDeviceSummary = [NSString stringWithFormat:@"\n\n%@ %@ (%@)\n%@ (%@)",
                                  [self appName],
                                  [self appVersionName],
                                  [self appBuildNumber],
                                  [UIDevice currentDevice].model,
                                  [UIDevice currentDevice].systemVersion];
    [self setSupportEmailMessage:appDeviceSummary forFeedbackType:LNTXFeedbackTypeConfused];
    [self setSupportEmailMessage:appDeviceSummary forFeedbackType:LNTXFeedbackTypeUnhappy];
    
    sSupportEmailRecipients = [NSMutableDictionary dictionaryWithCapacity:3];
    [self setSupportEmailRecipients:@[@"suggestion@linitix.com"] forFeedbackType:LNTXFeedbackTypeHappy];
    [self setSupportEmailRecipients:@[@"help@linitix.com"] forFeedbackType:LNTXFeedbackTypeConfused];
    [self setSupportEmailRecipients:@[@"support@linitix.com"] forFeedbackType:LNTXFeedbackTypeUnhappy];
}

+ (void)showFeedbackActionSheetInView:(UIView *)view completion:(LNTXFeedbackActionSheetCompletion)completion {
    sActionSheetDelegate = [[LNTXFeedbackActionSheetDelegate alloc] init];
    sActionSheetDelegate.completion = completion;
    
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:LNTXLocalizedStringFromTable(@"feedback_humour_title_format",
                                                                                                                              kLocalizationTable),
                                                                       [self appName]]
                                                             delegate:sActionSheetDelegate
                                                    cancelButtonTitle:LNTXLocalizedStringFromTable(@"cancel", kLocalizationTable)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:LNTXLocalizedStringFromTable(@"feedback_humor_happy_title", kLocalizationTable), LNTXLocalizedStringFromTable(@"feedback_humor_confused_title", kLocalizationTable), LNTXLocalizedStringFromTable(@"feedback_humor_unhappy_title", kLocalizationTable), nil];
    [actionSheet showInView:view];
}

+ (instancetype)instantiateWithType:(LNTXFeedbackType)feedbackType {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LNTXAboutViewController"
                                                         bundle:nil];
    
    LNTXFeedbackTableViewController *viewController;
    
    switch (feedbackType) {
        case LNTXFeedbackTypeHappy:
            viewController = [storyboard instantiateViewControllerWithIdentifier:@"LNTXHappyFeedback"];
            break;
            
        case LNTXFeedbackTypeConfused:
            viewController = [storyboard instantiateViewControllerWithIdentifier:@"LNTXConfusedFeedback"];
            break;
            
        case LNTXFeedbackTypeUnhappy:
            viewController = [storyboard instantiateViewControllerWithIdentifier:@"LNTXUnhappyFeedback"];
            break;
            
    }
    
    viewController.feedbackType = feedbackType;
    
    return viewController;
}

+ (void)setSupportEmailSubject:(NSString *)subject forFeedbackType:(LNTXFeedbackType)feedbackType {
    sSupportEmailSubjects[@(feedbackType)] = [subject copy];
}

+ (NSString *)supportEmailSubjectForFeedbackType:(LNTXFeedbackType)feedbackType {
    return sSupportEmailSubjects[@(feedbackType)];
}

+ (void)setSupportEmailMessage:(NSString *)subject forFeedbackType:(LNTXFeedbackType)feedbackType {
    sSupportEmailMessages[@(feedbackType)] = [subject copy];
}

+ (NSString *)supportEmailMessageForFeedbackType:(LNTXFeedbackType)feedbackType {
    return sSupportEmailMessages[@(feedbackType)];
}

+ (void)setSupportEmailRecipients:(NSArray *)recipients forFeedbackType:(LNTXFeedbackType)feedbackType {
    sSupportEmailRecipients[@(feedbackType)] = [recipients copy];
}

+ (NSArray *)supportEmailRecipientsForFeedbackType:(LNTXFeedbackType)feedbackType {
    return sSupportEmailRecipients[@(feedbackType)];
}

+ (NSString *)appName {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}

+ (NSString *)appVersionName {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (NSString *)appBuildNumber {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.twitterMessage) {
        self.twitterMessage = [NSString stringWithFormat:LNTXLocalizedStringFromTable(@"feedback_share_default_twitter_message",
                                                                                      kLocalizationTable), [self.class appName]];
    }
    
    if (!self.facebookMessage) {
        self.facebookMessage = [NSString stringWithFormat:LNTXLocalizedStringFromTable(@"feedback_share_default_facebook_message",
                                                                                       kLocalizationTable), [self.class appName]];
    }
    
    
    if (!self.friendEmailSubject) {
        self.friendEmailSubject = [NSString stringWithFormat:LNTXLocalizedStringFromTable(@"feedback_share_default_email_subject",
                                                                                          kLocalizationTable),
                                   [self.class appName]];
    }
    
    if (!self.friendEmailMessage) {
        self.friendEmailMessage = [NSString stringWithFormat:LNTXLocalizedStringFromTable(@"feedback_share_default_email_message",
                                                                                          kLocalizationTable),
                                   [self.class appName],
                                   self.appStoreURL];
    }
}

- (void)setFeedbackType:(LNTXFeedbackType)feedbackType {
    _feedbackType = feedbackType;
    
    if (!self.supportEmailSubject) {
        self.supportEmailSubject = sSupportEmailSubjects[@(feedbackType)];
    }
    
    if (!self.supportEmailMessage) {
        self.supportEmailMessage = sSupportEmailMessages[@(feedbackType)];
    }
    
    if (!self.supportEmailRecipients) {
        self.supportEmailRecipients = sSupportEmailRecipients[@(feedbackType)];
    }
}

- (void)tweetWithMessage:(NSString*)message andURL:(NSURL*)URL {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *controller = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [controller setInitialText:message];
        [controller addURL:URL];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)facebookWithMessage:(NSString*)message andURL:(NSURL*)URL {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:message];
        [controller addURL:URL];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}

- (void)mailWithSubject:(NSString*)subject message:(NSString*)message toRecipients:(NSArray*)recipients {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:subject];
        [controller setMessageBody:message isHTML:NO];
        [controller setToRecipients:recipients];
        [self presentViewController:controller
                           animated:YES
                         completion:nil];
    } else {
        // Handle the error
    }
}

- (void)openAppStoreForReview {
    [[UIApplication sharedApplication] openURL:self.appStoreURL];
}

- (void) sendHappyFriendMail {
    [self mailWithSubject:self.friendEmailSubject
                  message:self.friendEmailMessage
             toRecipients:nil];
}

- (void)sendSupportEmail {
    [self mailWithSubject:self.supportEmailSubject
                  message:self.supportEmailMessage
             toRecipients:self.supportEmailRecipients];
}

#pragma mark - UITableViewDatasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        switch (self.feedbackType) {
            case LNTXFeedbackTypeHappy:
                return LNTXLocalizedStringFromTable(@"feedback_contact_happy_header_title", kLocalizationTable);
                
            case LNTXFeedbackTypeConfused:
                return LNTXLocalizedStringFromTable(@"feedback_contact_confused_header_title", kLocalizationTable);
                
            case LNTXFeedbackTypeUnhappy:
                return LNTXLocalizedStringFromTable(@"feedback_contact_unhappy_header_title", kLocalizationTable);
        }
    } else if (section == 1){
        return LNTXLocalizedStringFromTable(@"feedback_share_header_title", kLocalizationTable);
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (self.feedbackType){
        case LNTXFeedbackTypeHappy:
            if (indexPath.section == 0) {
                if (indexPath.row == 0) {
                    [self openAppStoreForReview];
                } else if (indexPath.row == 1) {
                    [self sendSupportEmail];
                }
            } else if (indexPath.section == 1) {
                if (indexPath.row == 0) {
                    [self tweetWithMessage:self.twitterMessage
                                    andURL:self.appStoreURL];
                } else if (indexPath.row == 1) {
                    [self facebookWithMessage:self.facebookMessage
                                       andURL:self.appStoreURL];
                } else if (indexPath.row == 2) {
                    [self sendHappyFriendMail];
                }
            }
            break;
        case LNTXFeedbackTypeConfused:
        case LNTXFeedbackTypeUnhappy:
            if (indexPath.section == 0 && indexPath.row == 0) {
                [self sendSupportEmail];
            }
            break;
    }
}

//- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
//    if([view isKindOfClass:[UITableViewHeaderFooterView class]]) {
//        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
//        tableViewHeaderFooterView.textLabel.text = [self capitalizeString:tableViewHeaderFooterView.textLabel.text];
//    }
//}

#pragma mark - mail composer

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - string processor

- (NSString*)capitalizeString:(NSString *)stringToProcess {
    NSMutableString *processedString = [[stringToProcess lowercaseString] mutableCopy];
    NSString *pattern = @"(^|\\.|\\?|\\!)\\s*(\\p{Letter})";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:NULL];
    [regex enumerateMatchesInString:stringToProcess
                            options:0
                              range:NSMakeRange(0, [stringToProcess length])
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        NSRange r = [result rangeAtIndex:2];
        [processedString replaceCharactersInRange:r
                                       withString:[[stringToProcess substringWithRange:r] uppercaseString]];
    }];
    return [processedString copy];
}

@end

@implementation LNTXFeedbackActionSheetDelegate

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.cancelButtonIndex == buttonIndex) {
        self.completion(nil);
        return;
    }
    
    LNTXFeedbackType selectedType;
    if (buttonIndex == [actionSheet firstOtherButtonIndex]) {
        selectedType = LNTXFeedbackTypeHappy;
    } else if (buttonIndex == [actionSheet firstOtherButtonIndex] + 1) {
        selectedType = LNTXFeedbackTypeConfused;
    } else {
        selectedType = LNTXFeedbackTypeUnhappy;
    }
    
    self.completion([LNTXFeedbackTableViewController instantiateWithType:selectedType]);
}

@end
