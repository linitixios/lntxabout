//
//  main.m
//  LNTXAbout
//
//  Created by Damien Rambout on 24/03/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LNTXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LNTXAppDelegate class]));
    }
}
