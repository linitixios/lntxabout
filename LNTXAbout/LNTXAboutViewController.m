//
//  LNTXAboutViewController.m
//  LNTXAbout
//
//  Created by Damien Rambout on 24/03/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXAboutViewController.h"

#import <LNTXLocalizationKit/LNTXLocalizationKit.h>

@import MessageUI;

@interface LNTXAboutViewController ()
<MFMailComposeViewControllerDelegate>

@end

@implementation LNTXAboutViewController

+ (instancetype)instantiate {
    return [[UIStoryboard storyboardWithName:@"LNTXAboutViewController" bundle:nil] instantiateInitialViewController];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGRect headerFrame = self.tableView.tableHeaderView.frame;
    CGFloat maxY = 0;
    for (UIView *subView in self.tableView.tableHeaderView.subviews) {
        maxY = MAX(maxY, CGRectGetMaxY(subView.frame));
    }
    
    if (headerFrame.size.height != maxY + 7.0) {
        headerFrame.size.height = maxY + 7.0;
        self.tableView.tableHeaderView.frame = headerFrame;
        self.tableView.tableHeaderView = self.tableView.tableHeaderView;
    }
}

- (void)mailWithSubject:(NSString*)subject message:(NSString*)message toRecipients:(NSArray*)recipients {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:subject];
        [controller setMessageBody:message isHTML:NO];
        [controller setToRecipients:recipients];
        [self presentViewController:controller
                           animated:YES
                         completion:nil];
    } else {
        // Handle the error
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) { // Web
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.linitix.com"]];
    } else if (indexPath.row == 1) { // Twitter
        NSURL *nativeURL = [NSURL URLWithString:@"twitter://user?id=228038122"];
        if ([[UIApplication sharedApplication] canOpenURL:nativeURL]) {
            [[UIApplication sharedApplication] openURL:nativeURL];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/account/redirect_by_id?id=228038122"]];
        }
    } else if (indexPath.row == 2) { // Facebook
        NSURL *nativeURL = [NSURL URLWithString:@"fb://profile/413589145437564"];
        if ([[UIApplication sharedApplication] canOpenURL:nativeURL]) {
            [[UIApplication sharedApplication] openURL:nativeURL];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/linitix"]];
        }
    } else if (indexPath.row == 3) { // Contact
        [self mailWithSubject:[NSString stringWithFormat:LNTXLocalizedStringFromTable(@"about_contact_email_subject",
                                                                                      @"LNTXAbout.local"), [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]]
                      message:@""
                 toRecipients:@[@"info@linitix.com"]];
    }
}

#pragma mark - mail composer

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
