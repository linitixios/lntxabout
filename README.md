# LNTXCoreKit

`LNTXAbout` provides view controller that displays information about **LINITIX**.

## Installation

Make sure you have added the private **LINITIX** CocoaPods-Specs repository to your CocoaPods repository. To add it, run the following command in your terminal:

```
$ pod repo add LINITIX https://bitbucket.org/linitixios/cocoapods-specs.git
```

LNTXCoreKit is available through **LINITIX**'s private CocoaPods repository. To install
it simply add the following line to your Podfile:

```
pod "LNTXAbout"
```

## Author

Damien Rambout, damien.rambout@linitix.com

*LINITIX All Rights Reserved.*


